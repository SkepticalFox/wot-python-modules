﻿''' SkepticalFox 2019 '''

import os
import json
import shutil
import code_generator


try:shutil.rmtree('build')
except:pass

try:os.mkdir('build')
except:pass



# opening

with open('data/wotmodules.h.template', 'r') as f:
    wotmodules_h = f.read()

with open('data/wotmodules.cpp.template', 'r') as f:
    wotmodules_cpp = f.read()

with open('data/BigWorld.methods.json', 'r') as f:
    methods = json.load(f)



# code generation

code_gen = code_generator.CodeGen()

for method in methods.items():
    code_gen.add('BigWorld', method)

wotmodules_h += code_gen.h_file_body
wotmodules_cpp += code_gen.cpp_file_body

# end BW namespace
wotmodules_h += '\n}\n'



# saving

with open('build/wotmodules.h', 'w') as f:
    f.write(wotmodules_h)


with open('build/wotmodules.cpp', 'w') as f:
    f.write(wotmodules_cpp)
