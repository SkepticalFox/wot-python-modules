''' SkepticalFox 2019 '''

from collections import defaultdict


class CodeGen:
    def __init__(self):
        self.h_methods = defaultdict(list)

    def add(self, ns, method):
        method_name, value = method

        if value['result'] is not None:
            result = value['result']['type']
        else:
            result = 'void'

        args = []
        for arg_name, param_value in value['parameters'].items():
            if isinstance(param_value['type'], str):
                arg_type = param_value['type']
            else:
                arg_type = 'void*'
            args.append(f'{arg_type} {arg_name}')

        method_args = ', '.join(args)
        h_method = f'{result} {method_name}({method_args});'

        self.h_methods[ns].append(h_method)

    @property
    def h_file_body(self):
        res = ''
        for ns, methods in self.h_methods.items():
            res += f'namespace {ns} {{\n'
            res += '\n'.join(methods)
            res += '\n}\n'
        return res

    @property
    def cpp_file_body(self):
        res = ''
        for ns in self.h_methods.keys():
            res += f'using namespace {ns};\n'
        return res
